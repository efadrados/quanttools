// Copyright (C) 2018 Ernesto Fernández
//
// This file is part of QuantTools.
//
// This code is only an adaptation
// from the indicator ZigZag in the package TTR, you can found it in
// https://github.com/joshuaulrich/TTR
//
// QuantTools is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// QuantTools is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with QuantTools. If not, see <http://www.gnu.org/licenses/>.

#ifndef ZIGZAG_H
#define ZIGZAG_H

#include "Rcpp.h"
#include "Indicator.h"
#include "../BackTest/Candle.h"
#include "../ListBuilder.h"
#include <vector>

class ZigZagValue {

public:

  double price;
  int index;

};

template< typename Input >
class ZigZag : public Indicator< Input, double, Rcpp::List > {

private:

  std::vector< double > zgzg;
  std::vector< double > zgchange;

  double z = NA_REAL;

  double prevhigh;
  double prevlow;

  double change;
  bool use_percent;
  bool use_retrace;
  bool use_last_ex;
  int i = -1;

  double extreme_min, extreme_max, local_min, local_max;
  int signal = 0;


  ZigZagValue reference, inflection;


  void Update( Candle candle )
  {
    calc( candle.high, candle.low ) ;
  }

  void Update( double value )
  {
    calc(value, value);
  }

  void calc(double high, double low)
  {

    i++;
    z = (high + low) / 2;

    if(i == 0) {

      reference.price = z;
      reference.index = i;
      prevhigh = high;
      prevlow = low;
      return;

    } else if(i == 1) {

      inflection.price = z;
      inflection.index = i;

    }

    if (use_percent) {

      /* If % change given (absolute move) */
      extreme_min = inflection.price * ( 1.0 - change );
      extreme_max = inflection.price * ( 1.0 + change );

    } else {

      /* If $ change given (only absolute moves make sense) */
      extreme_min = inflection.price - change;
      extreme_max = inflection.price + change;

    }

    /* Find local maximum and minimum */
    local_max = inflection.price > high ? inflection.price : high;
    local_min = inflection.price < low ? inflection.price : low;

    /* Find first trend */
    if (signal == 0) {

      if (use_retrace) {

        /* Retrace prior move */
        signal = (inflection.price >= reference.price) ? 1 : -1;

      } else {

        /* Absolute move */
        if (local_min <= extreme_min) {
          /* Confirmed Downtrend */
          signal = -1;

        }

        if (local_max >= extreme_max) {
          /* Confirmed Uptrend */
          signal = 1;

        }
      }
    }

    /* Downtrend */
    if (signal == -1) {

      /* New Minimum */
      if (low == local_min) {

        /* Last Extreme */
        if (use_last_ex) {

          inflection.price = low;
          inflection.index = i;

        } else {

          /* First Extreme */
          if (low != prevlow) {

            inflection.price = low;
            inflection.index = i;

          }
        }
      }

      /* Retrace prior move */
      if (use_retrace) {

        extreme_max = inflection.price + ((reference.price - inflection.price) * change);

      }

      /* Trend Reversal */
      if (high >= extreme_max) {

        zgchange[reference.index] = -1*signal;
        zgzg[reference.index] = reference.price;
        reference = inflection;
        inflection.price = high;
        inflection.index = i;
        signal = 1;
        prevhigh = high;
        prevlow = low;

        return;

      }
    }

    /* Uptrend */
    if (signal == 1) {

      /* New Maximum */
      if (high == local_max) {

        /* Last Extreme */
        if (use_last_ex) {

          inflection.price = high;
          inflection.index = i;

        } else {

          /* First Extreme */
          if (high != prevhigh) {

            inflection.price = high;
            inflection.index = i;

          }
        }
      }

      /* Retrace prior move */
      if (use_retrace) {

        extreme_min = inflection.price - ((inflection.price - reference.price) * change);

      }

      /* Trend Reversal */
      if (low <= extreme_min) {

        zgchange[reference.index] = -1*signal;
        zgzg[reference.index] = reference.price;
        reference = inflection;
        inflection.price = low;
        inflection.index = i;
        signal = -1;
        prevhigh = high;
        prevlow = low;

        return;

      }
    }

    prevhigh = high;
    prevlow = low;

  }


  std::vector<double> linearInterpolate(std::vector<double> x)
  {

    int x1 = 0;
    double y1 = NA_REAL;
    int x2 = 0;
    double y2 = NA_REAL;

    for(int i = 0; i< x.size(); i++) {

      if(std::isnan(y1) && !std::isnan(x[i])) {

        x1 = i;
        y1 = x[i];

      } else if(!std::isnan(y1)) {

        if(!std::isnan(x[i]) && x1 < i) {

          x2 = i;
          y2 = x[i];

          for(int j = (x1+1); j<x2; j++) {

            x[j] = ( ( j - x1 )*( y2 - y1) / ( x2 - x1) ) + y1;

          }

          x1 = x2;
          y1 = y2;

        }
      }

    }

    return x;
  }

public:

  ZigZag( double _change, bool percent, bool retrace, bool lastExtreme ) :

  change( (double)_change ),
  use_percent( ( bool )percent ),
  use_retrace( ( bool )retrace ),
  use_last_ex( ( bool )lastExtreme )
  {

    if (use_percent) {

      change = change / 100.0;

    }

  }

  void Add( Input value )
  {

    zgzg.push_back(NA_REAL);
    zgchange.push_back(NA_REAL);
    Update( value );

  }

  bool IsFormed() { return zgzg.size() >= 2; }

  double GetValue() { return z; }

  Rcpp::List GetHistory() {

    std::vector< double > ret(zgzg);

    ret[reference.index] = reference.price;
    ret[inflection.index] = inflection.price;

    ret = linearInterpolate(ret);

    Rcpp::List history = ListBuilder().AsDataTable()
                                      .Add( "zgzg", ret )
                                      .Add( "zgchg", zgchange );
    return history;

  }

  void Reset()
  {

    z = NA_REAL;
    std::vector< double > empty;
    std::swap( zgzg, empty );

  }
};

#endif //ZIGZAG_H
