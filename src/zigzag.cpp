// Copyright (C) 2018 Ernesto Fernández
//
// This file is part of QuantTools.
//
// QuantTools is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// QuantTools is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with QuantTools. If not, see <http://www.gnu.org/licenses/>.

#include "../inst/include/Indicators/ZigZag.h"
#include "../inst/include/Utils.h"
#include <Rcpp.h>

//' ZigZag
//'
//' @param x \code{high, low} or \cod{close} data.frame
//' @param change Minimum price movement, either in money or percent
//' @param percent Use percentage or money change
//' @param change is a retracement of the previous move, or an absolute change from peak to trough
//' @param lastExtreme If the extreme price is the same over multiple periods, should the extreme price be the first or last observation
//' @family technical indicators
//' @return data.table with columns \code{zgzg, zgchange}
//' @description Zig Zag higlights trends by removing price changes smaller than change.
//' The Zig Zag is non-predictive.  The purpose of the Zig Zag is filter noise
//' and make chart patterns clearer.  It's more a visual tool than an indicator.
//' @export
// [[Rcpp::export]]
Rcpp::List zigzag( SEXP x, double change = 10.0, bool percent = true, bool retrace = false, bool lastExtreme = true ) {

  switch( TYPEOF( x ) ) {

    case REALSXP: {

      Rcpp::NumericVector vec = Rcpp::as< Rcpp::NumericVector >( x );

      ZigZag<double> zigzag( change, percent, retrace, lastExtreme );

      for( auto i = 0; i < vec.size(); i++ ) zigzag.Add( vec[i] );

      return zigzag.GetHistory();

    }
    case VECSXP: {

      Rcpp::DataFrame hlc = Rcpp::as< Rcpp::DataFrame >( x );
      Rcpp::StringVector names = hlc.attr( "names" );

      bool hasHigh   = std::find( names.begin(), names.end(), "high"   ) != names.end();
      bool hasLow  = std::find( names.begin(), names.end(), "low"  ) != names.end();

      if( !hasHigh   ) throw std::invalid_argument( "candles must contain 'high' column"   );
      if( !hasLow  ) throw std::invalid_argument( "candles must contain 'low' column"  );

      Rcpp::NumericVector highs  = hlc[ "high"   ];
      Rcpp::NumericVector lows   = hlc[ "low"    ];

      ZigZag<Candle> zigzag( change, percent, retrace, lastExtreme );

      for( auto i = 0; i < highs.size(); i++ ) {
        Candle candle( 1 );
        candle.low   = lows  [i];
        candle.high  = highs [i];

        zigzag.Add( candle );
      }

      return zigzag.GetHistory();

    }

  }

  Rcpp::List zerolist;

  return zerolist;
}
//' @name zigzag
